const canvas = document.getElementById('map');
const ctx = canvas.getContext('2d');

ctx.fillStyle = 'green';
ctx.fillRect(0, 0, 500, 500);


// 开始游戏
function start() {
    theFood = new Food('red');
    theFood.initRandom();
    mySnake = new Snake('blue');
    mySnake.init();
    window.setInterval(uniformMove, 500); // 通过计时器实现蛇的匀速移动
}
// 匀速移动
function uniformMove() {
    let ate = false; // 当前的食物是否被吃
    let direction = mySnake.direction;
    if(direction === 'up'){ // 上
        if(mySnake.body[0].y-10 === theFood.y && mySnake.body[0].x === theFood.x) {
            ate = eat(mySnake,theFood,ate)
        }
        initMap();
    }
    if(direction === 'down'){ //下
        if(mySnake.body[0].y+10 === theFood.y && mySnake.body[0].x === theFood.x) {
            ate = eat(mySnake,theFood,ate)
        }
        initMap();
    }
    if(direction === 'left'){ //左
        if(mySnake.body[0].x-10 === theFood.x && mySnake.body[0].y === theFood.y) {
            ate = eat(mySnake,theFood,ate)
        }
        initMap();
    }
    if(direction === 'right'){ //右
        if(mySnake.body[0].x+10 === theFood.x && mySnake.body[0].y === theFood.y) {
            ate = eat(mySnake,theFood,ate)
        }
        initMap();
    }
    mySnake.moveUniform();
    // 判断食物是否被吃
    if(ate) {
        theFood.initRandom()
        ate = false;
    } else {
        theFood.init()
    }
}
// 清空整个地图（包括食物和蛇），重新绘制
function initMap() {
    ctx.fillStyle = 'green';
    ctx.fillRect(0, 0, 500, 500);
}
// 蛇吃食物的同时需要增加一节蛇身并清空当前的食物
function eat(mySnake,theFood,ate) {
    let tail = mySnake.body[mySnake.body.length-1];
    mySnake.body.push(tail);// 增加尾部
    theFood.clear(); // 清除食物
    ate = true;
    return ate
}
// 键盘点击事件
document.onkeydown = function(event) {
    let e = event || window.event || arguments.callee.caller.arguments[0];
    if(![37, 38, 39, 40].includes(e.keyCode)){
        return;
    }
    let ate = false; // 当前的食物是否被吃
    if(e.keyCode==38){ // 上
        mySnake.direction = 'up'; // 设置蛇的移动方向
        if(mySnake.body[0].y-10 === theFood.y && mySnake.body[0].x === theFood.x) {
            ate = eat(mySnake,theFood, ate)
        }
        initMap();
    }
    if(e.keyCode==40){ //下
        mySnake.direction = 'down';
        if(mySnake.body[0].y+10 === theFood.y && mySnake.body[0].x === theFood.x) {
            ate = eat(mySnake,theFood,ate)
        }
        initMap();
    }
    if(e.keyCode==37){ //左
        mySnake.direction = 'left';
        if(mySnake.body[0].x-10 === theFood.x && mySnake.body[0].y === theFood.y) {
            ate = eat(mySnake,theFood,ate)
        }
        initMap();
    }
    if(e.keyCode==39){ //右
        mySnake.direction = 'right';
        if(mySnake.body[0].x+10 === theFood.x && mySnake.body[0].y === theFood.y) {
            ate = eat(mySnake,theFood,ate)
        }
        initMap();
    }
    mySnake.move(e);
    if(ate) {
        theFood.initRandom()
        ate = false;
    } else {
        theFood.init()
    }
}
// 延时0.5秒自动开始游戏 （可设置按钮，通过点击按钮触发）
window.setTimeout(start, 500);
