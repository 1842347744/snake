// 构造条蛇
function Snake(color){
    this.x = 200;
    this.y = 200;
    this.sw = 10;
    this.sh = 10;
    this.direction = 'left';
    // this.speed = speed;
    this.color = color;
    this.body = [
        {x:this.x,y:this.y},
        {x:this.x+10,y:this.y},
        {x:this.x+20,y:this.y},
    ]
}
// 蛇变身 
Snake.prototype.init = function() {
    ctx.fillStyle = this.color;
    this.body.forEach(item=>{
        ctx.fillRect(item.x, item.y, this.sw, this.sh);
    })
}
// 移动
Snake.prototype.move = function(e) {
    if(e.keyCode==38){ // 上
        let head = {
            x: this.body[0].x,
            y: this.body[0].y - 10
        }
        this.body.unshift(head);
        this.body = newbody(this.body);
        this.init();
    }
    if(e.keyCode==40){ //下
        let head = {
            x: this.body[0].x,
            y: this.body[0].y + 10
        }
        this.body.unshift(head);
        this.body = newbody(this.body);
        this.init();
    }
    if(e.keyCode==37){ //左
        let head = {
            x: this.body[0].x-10,
            y: this.body[0].y
        }
        this.body.unshift(head);
        this.body = newbody(this.body);
        this.init();
    }
    if(e.keyCode==39){ //右
        let head = {
            x: this.body[0].x+10,
            y: this.body[0].y
        }
        this.body.unshift(head);
        this.body = newbody(this.body);
        this.init();
    }
}
// 蛇匀速移动
Snake.prototype.moveUniform = function() {
    if(this.direction === 'up'){ // 上
        let head = {
            x: this.body[0].x,
            y: this.body[0].y - 10
        }
        this.body.unshift(head);
        this.body = newbody(this.body);
        this.init();
    }
    if(this.direction === 'down'){ //下
        let head = {
            x: this.body[0].x,
            y: this.body[0].y + 10
        }
        this.body.unshift(head);
        this.body = newbody(this.body);
        this.init();
    }
    if(this.direction === 'left'){ //左
        let head = {
            x: this.body[0].x-10,
            y: this.body[0].y
        }
        this.body.unshift(head);
        this.body = newbody(this.body);
        this.init();
    }
    if(this.direction === 'right'){ //右
        let head = {
            x: this.body[0].x+10,
            y: this.body[0].y
        }
        this.body.unshift(head);
        this.body = newbody(this.body);
        this.init();
    }
}
// 新的身体
function newbody(body) {
    let newbody = [];
    for (let i = 0; i < body.length-1; i++){
        newbody.push(body[i])
    }
    return newbody;
}