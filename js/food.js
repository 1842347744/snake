// 构造个食物
function Food(color) {
    this.x = 0; // 食物x坐标
    this.y = 0; // 食物y坐标
    this.fw = 10; // 食物的宽 10px
    this.fh = 10; // 食物的高 10px
    this.color = color;
}
// 食物正常生成
Food.prototype.init = function() {
    ctx.fillStyle = this.color;
    ctx.fillRect(this.x, this.y, this.fw, this.fh);
}
// 食物随机生成
Food.prototype.initRandom = function() {
    this.x = Math.floor(Math.random()*50)*10;
    this.y = Math.floor(Math.random()*50)*10;
    ctx.fillStyle = this.color;
    ctx.fillRect(this.x, this.y, this.fw, this.fh);
}
// 食物被干了
Food.prototype.clear = function() {
    ctx.clearRect(this.x, this.y, this.fw, this.fh);
}